﻿module dygren.utils.time;

struct Frames
{
    uint frame;
    enum  all = 16;

    void reset () { this.frame = all; }
    void reduce () { this.frame--; }

    void setZero () { this.frame = 0; }
    void setTwice () { this.frame = twice; }
    void setTriple () { this.frame = triple; }
    void setQuater () { this.frame = quarter; }
    void setHalf () { this.frame = half; }
    void setThreeQuaters () { this.frame = threeQuaters; }
    void setTime (float time) { this.frame = times(time); }
    void setQuant (float quant) { this.frame = quants(quant); }
    
@property: 
    
    auto isZero () { return this.frame == 0; }
    auto isQuarter () { return this.frame == quarter; }
    auto isThreeQuaters () { return this.frame == all - quarter; }
    auto isHalf () { return this.frame == half; }
    auto isStep(uint step) { return this.frame%step == 0; }
    auto isTime(float time) { return this.frame == cast (uint) all*time; }
    auto isQuant(float Quant) { return this.frame == cast (uint) all/Quant; }
    
static:
    uint zero () { return 0; }

    uint twice () { return all * 2; }
    uint triple () { return all * 3; }
    uint times (float time) { return cast(uint) (all * time); }

    uint quarter () { return all / 4; }
    uint half () { return all / 2; }
    uint threeQuaters () { return cast(uint) (quarter * 3); }
    uint quants (float quant) { return cast(uint) (all / quant); }
}