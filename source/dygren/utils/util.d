﻿module dygren.utils.util;

private
{
    import std.conv: parse;
    import std.array: array;
    import std.range: chunks;
    import std.algorithm: map;
}

// Compile-time test for char existence in the string.
@safe bool hasSymbol (S)(string str, S sym) pure nothrow
    if (is (S == char) || is (S == string))
{
    bool has;
    
    foreach (ref letter; str)
        if (letter == sym)
            has = true;
    
    return has;
}

bool eachMatch (T)(T[] list, bool delegate(ref T) condition)
{
    bool result;
    foreach (ref element; list)
        if (condition (element))
            result = true;

    return result;
}

ubyte[3] hex2rgb (string hex)
{
    ubyte[3] result = hex.chunks (2)
        .map!(twoDigits => twoDigits.parse!ubyte (16))
        .array ();
    
    return result;
}

template memberExists (Type, string memberName)
{
    enum memberExists = result ();

    private bool result ()
    {
        static if (is (Type == struct) || is (Type == class))
        {
            foreach (member; __traits (allMembers, Type))
                if (memberName == member)
                    return true;
        }
        
        return false;
    }
}


struct OGLAxes
{
    static OGLAxis x;
    static OGLAxis y;
    static OGLAxis z;
}

private struct OGLAxis
{
    enum start = -1.0;
    enum end = 1.0;
    enum length = end - start;
}