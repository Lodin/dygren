﻿module dygren.utils.refshelf;

mixin template RefShelf (Type)
{
    private
    {
        debug import dygren.utils.shelf : ShelfException,
            SHELF_ELEMENT_NOT_EXIST;
        
        import std.array : RefAppender, appender;
        debug import std.string : format;
    }
    
    private 
    {
        Type[] data;
        RefAppender!(Type[]) refdata;
    }
    
    @property 
    {
        size_t count () { return data.length; }
        ref Type[] all () { return data; }
    }
    
    ref Type opIndex (size_t number) { return data[number]; }
    
    void add (ChildType : Type)(ChildType[] objects...)
    {
        init ();
        refdata.put (objects);
    }
    
    void set (ChildType : Type)(ChildType[] objects...)
    {
        init ();
        refdata.put (objects);
    }
    
    void remove (size_t number)
    {
        import std.algorithm : remove;
        
        debug if (number >= count)
            throw new ShelfException (format (SHELF_ELEMENT_NOT_EXIST, number));
        
        data = data.remove (number);
    }
    
    void clear () { data.destroy (); }
    
    int opApply (int delegate (ref size_t, ref Type) foreach_)
    {
        int result;
        
        foreach (size_t index, ref element; data)
        {
            result = foreach_ (index, element);
            if (result) break;
        }
        
        return result;
    }
    
    int opApply (int delegate (ref Type) foreach_)
    {
        int result;
        
        foreach (ref element; data)
        {
            result = foreach_ (element);
            if (result) break;
        }
        
        return result;
    }
    
    void each (void delegate(ref Type) action)
    {
        foreach (ref element; data)
            action (element);
    }
    
private:
    
    void init ()
    {
        if (count > 0)
            return;
        
        refdata = appender (&data);
    }
}