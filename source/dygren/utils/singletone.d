﻿module dygren.utils.singletone;

private import std.traits : Unqual;

mixin template Singletone (Type)
    if (is (Unqual!Type == class))
{
    private static Type _instance;
    
    static @property Type instance ()
    {
        if (_instance is null)
            _instance = new Type;
        
        return _instance;
    }
}