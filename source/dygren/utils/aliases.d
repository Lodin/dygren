﻿module dygren.utils.aliases;

private
{
    import dygren.objects.gameobject : GameObject;
    import mathed : Vector;
}

alias void delegate(GameObject) Action;
alias bool delegate(GameObject) Condition;

alias Vector!(2, float, "start,end") Limits;
alias Size(Type) = Vector!(2, Type, "wh");