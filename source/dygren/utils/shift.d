﻿module dygren.utils.shift;

private import std.string : format;

struct Shift
{
    mixin (ShiftImpl!("active", "activate", "deactivate"));
}

template ShiftImpl (string Value, string Activation, string Deactivation)
{
    enum ShiftImpl = format (ShiftStr, Value, Activation, Deactivation);

    private enum ShiftStr = q{
        private bool _%1$s;
        
        @property bool %1$s () { return _%1$s; }
        
        void %2$s () { _%1$s = true; }
        void %3$s () { _%1$s = false; }
    };
}