﻿module dygren.utils.shelfrange;

mixin template ShelfRange ()
{
    private size_t _front;

    @property
    {
        ref auto front () { return data.data[_front]; }
        void front (size_t number) { _front = number; }

        bool empty () { return _front >= count; }
    }

    void popFront () { _front++; }
    void popBack () { _front--; }
    void reset () { _front = 0; }
}

mixin template NamedShelfActive (Type)
{
    debug private 
    {
        import dygren.utils.shelf : ShelfException,
            SHELF_ELEMENT_NOT_EXIST;
        import std.algorithm : canFind;
        import std.string : format;
    }

    private Type* _active;
    
    @property 
    {
        ref Type active () { return *_active; }
        
        void active (string name)
        {
            debug if (!list.canFind (name))
                throw new ShelfException (format (SHELF_ELEMENT_NOT_EXIST, name));

            _active = &data[name];
        }
    }
}