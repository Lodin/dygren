﻿module dygren.utils.shelf;

private
{
    import dygren.utils.exceptions : DygrenException;
    import std.traits : hasMember;
}

mixin template Shelf (Type)
{
    private
    {
        debug import dygren.utils.shelf : ShelfException,
            SHELF_ELEMENT_NOT_EXIST;

        import std.array : Appender, appender;
        import std.string : format;
    }

    private Appender!(Type[]) data;

    @property 
    {
        size_t count () { return data.data.length; }
        Type[] all () { return data.data; }
    }

    ref Type opIndex (size_t number) { return data.data[number]; }

    void add (ChildType : Type)(ChildType[] objects...) { push (object); }
    void set (ChildType : Type)(ChildType[] objects...)
    {
        data = appender (objects);

        static if (hasMember!(Type, "_front") && hasMember!(Type, "empty"))
            front = count - 1;
    }

    private void push (ChildType : Type)(ChildType[] objects...)
    {
        data.put (objects);

//        static if (hasMember!(Type, "_front") && hasMember!(Type, "empty"))
//            front = count - 1;
    }

    void remove (size_t number)
    {
        import std.algorithm : remove;

        debug if (number >= count)
            throw new ShelfException (format (SHELF_ELEMENT_NOT_EXIST, number));

        data = appender (data.data.remove (number));
    }

    void clear () { data.clear (); }

    int opApply (int delegate (ref size_t, ref Type) foreach_)
    {
        int result;
        
        foreach (size_t index, ref element; data.data)
        {
            result = foreach_ (index, element);
            if (result) break;
        }
        
        return result;
    }
    
    int opApply (int delegate (ref Type) foreach_)
    {
        int result;
        
        foreach (ref element; data.data)
        {
            result = foreach_ (element);
            if (result) break;
        }
        
        return result;
    }

    void each (void delegate(ref Type) action)
    {
        foreach (ref element; data.data)
            action (element);
    }
}

mixin template NamedShelf (Type)
{
    private
    {
        debug
        {
            import dygren.utils.shelf : ShelfException,
                SHELF_ELEMENT_NOT_EXIST, SHELF_ELEMENT_ALREADY_EXIST;
            
            import std.algorithm : canFind;
            import std.string : format;
        }

        import dygren.utils.util : memberExists;
    }

    Type[string] data;

    @property
    {
        size_t count () { return data.length; }
        string[] list () { return data.keys; }
    }

    ref Type opIndex (string name)
    {
        debug if (!list.canFind (name))
            throw new ShelfException (format (SHELF_ELEMENT_NOT_EXIST, name));

        return data[name];
    }

    ref Type opDispatch (string name)() { return opIndex (name); }

    protected void push (string name, Type element)
    {
        debug if (list.canFind (name))
            throw new ShelfException (format (SHELF_ELEMENT_ALREADY_EXIST, name));

        data[name] = element;

        static if (memberExists!(Type, "setInnerPointers"))
            data[name].setInnerPointers ();
    }

    void remove (string name)
    {
        import std.algorithm : remove;

        debug if (!list.canFind (name))
            throw new ShelfException (format (SHELF_ELEMENT_NOT_EXIST, name));

        data.remove (name);
    }

    void clear () { data.destroy (); }

    int opApply (int delegate (ref size_t, ref Type) foreach_)
    {
        int result;

        size_t index;
        foreach (ref element; data)
        {
            result = foreach_ (index, element);
            index++;
            if (result) break;
        }
        
        return result;
    }

    int opApply (int delegate (ref string, ref Type) foreach_)
    {
        int result;
        
        foreach (string name, ref element; data)
        {
            result = foreach_ (name, element);
            if (result) break;
        }
        
        return result;
    }
    
    int opApply (int delegate (ref Type) foreach_)
    {
        int result;
        
        foreach (ref element; data)
        {
            result = foreach_ (element);
            if (result) break;
        }
        
        return result;
    }

    void each (void delegate(ref Type) action)
    {
        foreach (ref element; data)
            action (element);
    }
}

mixin template OrderedNamedShelf (Type)
//    if (hasMember!(Type, "name"))
{
    private
    {
//        debug
//        {
            import dygren.utils.shelf : ShelfException,
                SHELF_ELEMENT_NOT_EXIST, SHELF_ELEMENT_ALREADY_EXIST;
            
            import std.array : Appender, appender;
            import std.algorithm : canFind;
            import std.string : format;
//        }
        
        import dygren.utils.util : memberExists;
    }
    
    Appender!(Type[]) data;
    
    @property
    {
        size_t count () { return data.data.length; }

        string[] list ()
        {
            string[] result;
            result.length = data.data.length;

            foreach (size_t index, ref element; data.data)
                result[index] = element.name;

            return result;
        }
    }
    
    ref Type opIndex (string name)
    {
        foreach (ref element; data.data)
            if (element.name == name)
                return element;

        throw new ShelfException (format (SHELF_ELEMENT_NOT_EXIST, name));
    }
    
    ref Type opDispatch (string name)() { return opIndex (name); }
    
    protected void push (Type element)
    {
        debug if (list.canFind (element.name))
            throw new ShelfException (format (SHELF_ELEMENT_ALREADY_EXIST, element.name));
        
        data.put (element);
        
        static if (memberExists!(Type, "setInnerPointers"))
            data.data[$ - 1].setInnerPointers ();
    }
    
    void remove (string name)
    {
        import std.algorithm : remove;

        foreach (size_t index, ref element; data.data)
            if (element.name == name)
                data.data.remove (index);

        throw new ShelfException (format (SHELF_ELEMENT_NOT_EXIST, name));
    }
    
    void clear () { data.clear (); }
    
    int opApply (int delegate (ref size_t, ref Type) foreach_)
    {
        int result;
        
        size_t index;
        foreach (ref element; data.data)
        {
            result = foreach_ (index, element);
            index++;
            if (result) break;
        }
        
        return result;
    }
    
    int opApply (int delegate (ref string, ref Type) foreach_)
    {
        int result;
        
        foreach (ref element; data.data)
        {
            result = foreach_ (element.name, element);
            if (result) break;
        }
        
        return result;
    }
    
    int opApply (int delegate (ref Type) foreach_)
    {
        int result;
        
        foreach (ref element; data.data)
        {
            result = foreach_ (element);
            if (result) break;
        }
        
        return result;
    }
    
    void each (void delegate(ref Type) action)
    {
        foreach (ref element; data.data)
            action (element);
    }
}

enum
{
    SHELF_ELEMENT_NOT_EXIST = "Specified element `%s` does not exist in shelf",
    SHELF_ELEMENT_ALREADY_EXIST = "Specified element `%s` is already exist in shelf"
}

class ShelfException : DygrenException 
{
    int code;

    this (string msg, int code = -1, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        this.code = code;
        super ("Shelf Exception: " ~ msg, file, line, next);
    }
}