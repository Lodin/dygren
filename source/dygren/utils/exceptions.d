﻿module dygren.utils.exceptions;

private
{
    import std.format : formattedWrite;
    import std.array : appender;
}

enum
{
    EMPTY_UNIFORM = 0xFFFFFFFF,

    LOOP_SDL_INIT_FAILED = "Unable to initialize SDL: '%s'",
    LOOP_WINDOW_CREATION_FAILED = "Unable to create window: '%s'",
    LOOP_WINDOW_NAME_NOT_DEFINED = "Window name is not defined",
    LOOP_WINDOW_SIZE_NOT_DEFINED = "Window size is not defined",
    LOOP_FLAG_NOT_SET = "SDL window flags are not set",
    
    SHADER_PROGRAM_CREATING_FAILED = "Error creating shader program",
    SHADER_OBJECT_CREATING_FAILED = "Error creating shader object with type %s",
    SHADER_COMPILING_FAILED = "Error compiling shader type '%s': '%s'",
    SHADER_PROGRAM_LINKING_FAILED = "Error shader program linking: '%s'",
    SHADER_PROGRAM_INVALID = "Error shader program validation: '%s'",
    SHADER_UNIFORM_EMPTY = "Shader uniform '%s' is empty",
    SHADER_UNIFORM_NO_VAR = "Specified uniform variable '%s' does not exists",

    SMALLRANGE_IS_EMPTY = "Current small range is already empty",

    TEXTURE_LOADING_FAILED = "Specified texture loading failed: '%s'",
    FONT_LIBRARY_LOADING_FAILED = "Library SDL_ttf loading failed: '%s'",
    FONT_FILE_LOADING_FAILED = "Specified font loading failed: '%s'",
    FONT_STYLE_NOT_EXIST = "Specified font style '%s' does not exist",
    FONT_RENDER_STRING_LENGTH_IS_NULL = "Specified render string has zero length",
    FONT_COLOR_NOT_ENOUGH = "Color hex string should have 6-symbol length, not %s",
    FONT_NOT_DEFINED = "Configuring font is not defined",

    KEY_DEFINED_BY_DEFAULT = "Specified key '%s' is already defined by default",
    KEY_ALREADY_DEFINED = "Specified key '%s' is already defined",
    KEY_NOT_BINDED = "Specified key '%s' does not defined yet",

    SOUND_NOT_FIND = "Not find music or sound effect",
    SOUND_NOT_INIT = "Not init sound",
}

class DygrenException : Exception
{
    this (string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super (msg, file, line, next);
    }
}

class ShaderException : DygrenException
{
    int code;

    this (string msg, int code = -1, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        this.code = code;
        super ("Shader Exception: " ~ msg, file, line, next);
    }
}

class TextureException : DygrenException
{
    int code;

    this (string msg, int code = -1, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        this.code = code;
        super ("Texture Exception: " ~ msg, file, line, next);
    }
}

class LoopException : DygrenException
{
    int code;

    this (string msg, int code = -1, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        this.code = code;
        super ("Loop Exception: " ~ msg, file, line, next);
    }
}

class KeyException : DygrenException
{
    int code;

    this (string msg, int code = -1, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        this.code = code;
        super ("Key Exception: " ~ msg, file, line, next);
    }
}

class FontException : DygrenException
{
    int code;
    
    this (string msg, int code = -1, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        this.code = code;
        super ("Font Exception: " ~ msg, file, line, next);
    }
}

class SoundException : DygrenException
{
    int code;
    
    this (string msg, int code = -1, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        this.code = code;
        super ("Sound Exception: " ~ msg, file, line, next);
    }
}