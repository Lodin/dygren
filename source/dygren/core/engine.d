﻿module dygren.core.engine;

private
{
    import dygren.core.loop : LoopController;
    import dygren.core.scene : SceneController;
    import dygren.core.key : KeyController;
    import dygren.sounds : Sounds;
    import dygren.utils.singletone : Singletone;
    import dygren.core.timer : Timer;
}

final class Engine
{
    mixin Singletone!Engine;

    LoopController loop;
    SceneController scene;
    KeyController key;
    Sounds sounds;
    Timer timer;

    private this ()
    {
        loop = new LoopController;
        scene = new SceneController;
        key = new KeyController;
        sounds = new Sounds;
        timer = new Timer;
    }
}

version (unittest)
{
    void main () {}
}