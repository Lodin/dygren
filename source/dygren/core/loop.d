﻿module dygren.core.loop;

private
{
    import dygren.core.scene : LayerSection;
    import dygren.utils.shelf : NamedShelf, Shelf;
    import dygren.utils.shelfrange : NamedShelfActive;
    import dygren.utils.time : Frames;
    import dygren.utils.exceptions;

    import core.thread : Thread, dur;
}

package:

final class LoopController
{
    bool running = true;
    States states;
    
    void run ()
    {
        while (running)
        {
            Thread.sleep (dur!"msecs" (Frames.all));
            running = states.exec ();
        }
    }
}

alias bool delegate() Action;

struct States
{
    mixin NamedShelf!State;
    mixin NamedShelfActive!State;

    void add (string name, Action action)
    {
        push (name, State (action));

        if (count == 1)
            this.active = name;
    }

    bool exec () { return this.active.action (); }
}

struct State
{
    Action action;

    private LayerSection* _section;

    this (Action action) { this.action = action; }

    @property ref LayerSection section () { return *_section; }
    void link (ref LayerSection section) { _section = &section; }
}