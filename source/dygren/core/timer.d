﻿module dygren.core.timer;

private
{ 
    import core.time : TickDuration;
}

class Timer
{
    private
    {
        TickDuration tickTime;
        long timeBeforeStop;
    }

    long time;
    bool timerRun;
    bool timerWait;

    this ()
    {
        timerRun = false;
        timerWait = false;
    }

    void check ()
    {
        if (timerRun && !timerWait)
        {
            auto timeNow = TickDuration.currSystemTick;
            time = timeNow.seconds - tickTime.seconds + timeBeforeStop;
        }
    }

    void run ()
    {
        time = 0;
        tickTime = TickDuration.currSystemTick;
        timeBeforeStop = 0;
        timerRun = true;
        timerWait = false;
    }

    void stop ()
    {
        auto timeNow = TickDuration.currSystemTick;
        timeBeforeStop = timeNow.seconds - tickTime.seconds + timeBeforeStop;
        timerWait = true;
    }

    void runAfterStop ()
    {
        tickTime = TickDuration.currSystemTick;
        timerWait = false;
    }

    void nullTime ()
    {
        time = 0;
    }

    @property long currentTime () { return time; }
}