﻿module dygren.core.key;

private
{
    import dygren.core.engine : Engine;
    import dygren.utils.shelf : NamedShelf;
    import dygren.utils.shelfrange : NamedShelfActive;
    import dygren.utils.util : hasSymbol;
    import dygren.utils.exceptions;

    import derelict.sdl2.sdl;

    import std.array : array;
    import std.string : toUpper, split, format;
    import std.algorithm : uniq, canFind;
    import std.conv : to;
}

package:

final class KeyController
{
    SDL_Event event;
    BindingStateList states;
    bool keyup;
    
    bool process ()
    {
        return states.active.action ();
    }
}

auto KeyNull_def = Key!"default" (-1, -1);

struct BindingStateList
{
    mixin NamedShelf!BindingState;
    mixin NamedShelfActive!BindingState;

    void add (string[] names...)
    {
        foreach (ref name; names)
        {
            push (name, BindingState ());
            
            if (count == 1)
                this.active = name;
            
            this[name].defaults.data =
            [
                "mouseleft_up"     :  Key!"default" (SDL_MOUSEBUTTONUP, SDL_BUTTON_LEFT),
				"mouseleft_down"   :  Key!"default" (SDL_MOUSEBUTTONDOWN, SDL_BUTTON_LEFT),
				"mouseright_up"    :  Key!"default" (SDL_MOUSEBUTTONUP, SDL_BUTTON_RIGHT),
                "mouseright_down"  :  Key!"default" (SDL_MOUSEBUTTONDOWN, SDL_BUTTON_RIGHT),
                "escape_keyup"     :  Key!"default" (SDL_KEYDOWN, SDLK_ESCAPE),
                "escape_keydown"   :  Key!"default" (SDL_KEYUP, SDLK_ESCAPE),
                "enter_keyup"      :  Key!"default" (SDL_KEYUP, SDLK_RETURN),
                "enter_keydown"    :  Key!"default" (SDL_KEYDOWN, SDLK_RETURN),
                "mousemotion"      :  Key!"default" (SDL_MOUSEMOTION, -1),
                "undefined"        :  Key!"default" (-1, -1)
            ];
        }
    }
}

struct BindingState
{
    UserKeys userkeys;
    DefaultKeys defaults;
    private bool isPressedKeyDefined;

    bool action ()
    {
        const Uint8* keystate = SDL_GetKeyboardState (null);
        auto event = &Engine.instance.key.event;
        
        while (SDL_PollEvent (event))
        {
            switch (event.type)
            { 
                case SDL_QUIT:
                    return false;

				case SDL_MOUSEBUTTONUP:
                case SDL_MOUSEBUTTONDOWN:
					mixin (KeyAction!("event.type", "event.button.button"));

                case SDL_KEYUP:
                case SDL_KEYDOWN:
                    mixin (KeyAction!("event.type", "event.key.keysym.sym"));

                case SDL_MOUSEMOTION:
                    mixin (KeyAction!("event.type", "-1"));
                
                case SDL_WINDOWEVENT:
                    switch (event.window.event)
                    {
                        case SDL_WINDOWEVENT_RESTORED:
                            SDL_SetWindowSize (Engine.instance.scene.window.ptr,Engine.instance.scene.window.size.w,Engine.instance.scene.window.size.h);
//                            Engine.scene.window.create();
                            break;
                        default:
                            break;
                    }
                    return true;
                default:
                    return true;
            }
        }

        foreach (string name, ref key; userkeys)
        {
            if (keystate[key.code])
            {
                if (key.action)
                    key.action ();

                isPressedKeyDefined = true;
                Engine.instance.key.keyup = false;
            }
        }

        if (!isPressedKeyDefined)
        {
            Engine.instance.key.keyup = true;

            if (defaults.undefined.action)
                defaults.undefined.action ();
        }

        isPressedKeyDefined = false;

        return true;
    }

    private template KeyAction (string parentCode, string code)
    {
        enum KeyAction = format (Action, parentCode, code);
        
        private enum Action = q{
            auto key = &defaults[%1$s, %2$s];
            if (key.isnull)
                break;
            if ((*key).action)
                return (*key).action ();
            else 
                break;
        };
    }

    debug private void setInnerPointers ()
    {
        userkeys.defaults = &defaults;
    }
}

struct UserKeys
{
    mixin NamedShelf!(Key!"user");

    debug private DefaultKeys* defaults;

    void add (string name)()
    {
        debug if (additionalKeys.values.canFind (name))
            throw new KeyException (format (KEY_DEFINED_BY_DEFAULT, name));
        
        debug if (data.values.canFind (name))
            throw new KeyException (format (KEY_ALREADY_DEFINED, name));

        push (name, Key!"user" (mixin ("SDL_SCANCODE_" ~ name.toUpper ())));
    }
    
    void set (string UsingKeys)()
        if (UsingKeys.hasSymbol ('|') || UsingKeys.hasSymbol (','))
    {
        debug 
        {
            string[] keys;
            
            if (UsingKeys.hasSymbol ('|'))
                keys = UsingKeys.split ("|");
            else
                keys = UsingKeys.split (",");
            
            foreach (ref key; keys)
                if (defaults.data.keys.canFind (key))
                    throw new KeyException (format (KEY_DEFINED_BY_DEFAULT, key));
        }
        
        data = mixin (generateKeylist (UsingKeys));
    }

    private static string generateKeylist (string UsingKeys)
    {
        string keylist = "[";
        string[] keys;
        
        if (UsingKeys.hasSymbol ('|'))
            keys = UsingKeys.split ("|");
        else
            keys = UsingKeys.split (","); 
        
        keys = array (uniq (keys));
        
        foreach (size_t index, ref key; keys)
        {
            keylist ~= "\"" ~ key ~ "\": Key!\"user\" (SDL_SCANCODE_" ~ key.toUpper () ~ ")";
            
            if (index < keys.length - 1)
                keylist ~= ", ";
        }
        
        return keylist ~ "]";
    }
}

struct DefaultKeys
{
    private Key!"default"[string] data;

    ref Key!"default" opIndex (string name)
    {
        debug if (!data.keys.canFind (name))
            throw new KeyException (format (KEY_NOT_BINDED, name));

        return data[name];
    }

    ref Key!"default" opIndex (int parentCode, int code)
    {
        foreach (ref key; data)
			if (key.parentCode == parentCode && key.code == code)
                return key;

        return KeyNull_def;
    }

    ref Key!"default" opDispatch (string name)()
    {
        return opIndex (name);
    }
}

struct Key (string Type = "user")
{
    static assert (isKeyTypeRight!Type, format (KEY_TYPE_NOT_RIGHT, Type));

    int code;
    static if (Type == "default")
        bool delegate() action = null;
    else
        void delegate() action = null;

    static if (Type == "default")
    {
        int parentCode;

        this (int parentCode, int code)
        {
            this.code = code;
            this.parentCode = parentCode;
        }

        bool isnull () { return code < 0 && parentCode < 0; }
    }
    else
    this (int code) { this.code = code; }
}

template isKeyTypeRight (string Type)
{
    enum isKeyTypeRight = Type == "default" || Type == "user";
}

enum KEY_TYPE_NOT_RIGHT = "Specified key should be `user` or `default`, not `%s`";