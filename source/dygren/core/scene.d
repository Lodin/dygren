﻿module dygren.core.scene;

private
{
    import dygren.core.engine : Engine;
    import dygren.objects.gameobject : GameObject;
    import dygren.graphics.glsl.base : baseShader;
    import dygren.graphics.libs : OGLib, SDLib, Window;
    import dygren.graphics.point : Vertex, VertexPoint, TexturePoint;
    import dygren.graphics.shader : Shader;
    import dygren.graphics.texture : Texture;
    import dygren.utils.shelf : Shelf, NamedShelf, OrderedNamedShelf;
    import dygren.utils.shift : ShiftImpl;
    import dygren.utils.util : OGLAxes;

    import mathed : Matrix4f, Planef;
    import derelict.opengl3.gl;

    import std.array : Appender;
}

//package:

final class SceneController
{
    Window window;
    OGLib oglib;
    SDLib sdlib;
    LayerList layers;
    ShaderList shaders;
    Attributes attributes;
    
    mixin (ShiftImpl!("locked", "lock", "unlock"));
    
    private auto mxworld = Matrix4f.identity;
    
    this ()
    {
        layers = LayerList (&shaders);
        shaders.add ("base", baseShader);
        
        oglib.load ();
        sdlib.load ();
        sdlib.init ();
    }
    
    void init ()
    {
        window.create ();
        window.context ();
        oglib.reload ();
        oglib.init ();
        shaders.init ();
    }
    
    void render ()
    {
        clear ();
        attributes.enable ();
        foreach (string name, ref layer; Engine.instance.loop.states.active.section)
            if (layer.isDefault)
                layer.render ();
        attributes.disable ();
        
        glFlush ();
        window.swap ();
    }
    
    void clear ()
    {
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        mxworld[0][3] = 0;
        mxworld[1][3] = 0;
        mxworld[2][3] = 0;
        mxworld[3][3] = 1;
    }

    bool getGameObject (GameObject gameObject, size_t mouseX, size_t mouseY, Texture levelTexture, float matrixX, float matrixY)
    {
        auto xVertexRight = gameObject.vertexPoints.upRight.x,
             xVertexLeft  = gameObject.vertexPoints.upLeft.x,
             yVertexUp = gameObject.vertexPoints.upRight.y,
             yVertexDown  = gameObject.vertexPoints.downRight.y;

        if (xVertexRight < OGLAxes.x.start || xVertexLeft > OGLAxes.x.end ||  yVertexUp < OGLAxes.y.start || yVertexDown > OGLAxes.y.end)
            return false;


        auto oglX = (OGLAxes.x.length / window.size.w) * mouseX - 1 - matrixX,
             oglY = -(OGLAxes.y.length / window.size.h) * mouseY + 1 - matrixY;

        if (oglX < xVertexLeft || oglX > xVertexRight || oglY < yVertexDown || oglY > yVertexUp)
            return false;

        auto vertexCoordsX = (oglX - xVertexLeft) / (xVertexRight - xVertexLeft),
            vertexCoordsY = (oglY - yVertexUp) / (yVertexDown - yVertexUp);

        auto up = gameObject.texturePoints.upRight.y,
            left = gameObject.texturePoints.upLeft.x,
            lengthX = gameObject.texturePoints.upRight.x - gameObject.texturePoints.upLeft.x,
            lengthY = gameObject.texturePoints.downRight.y - gameObject.texturePoints.upRight.y;

        auto alpha = levelTexture.getPixelAlpha (up, left, lengthX, lengthY, vertexCoordsX,vertexCoordsY);

        if (alpha == 0)
            return false;
        else
            return true;    
    }


}

enum
{
    SINGLE = 1,
    VEC2   = 2,
    VEC3   = 3,
    
    VERTEX_XYZ = 0,
    TEXTURE_XY = 1,
    ALPHA      = 2
}

struct ShaderList
{
    mixin NamedShelf!Shader;

    void init ()
    {
        foreach (ref shader; data)
        {
            shader.create ();
            shader.bind ();
            shader.compile ();
            shader.link ();

            debug shader.validate ();
            shader.use ();
            shader.test ();
        }
    }

    void add (string name, Shader shader)
    {
        push (name, shader);
    }
}

struct LayerList
{
    mixin NamedShelf!LayerSection;

    private ShaderList* shaders;
    
    this (ShaderList* shaders) { this.shaders = shaders; }

    void create (string[] names...)
    {
        foreach (ref name; names)
            push (name, LayerSection (shaders));
    }
}

struct LayerSection
{
    Planef matrix;
    mixin OrderedNamedShelf!Layer;
    
    private ShaderList* shaders;

    this (ShaderList* shaders) 
    { 
        this.shaders = shaders;    
        matrix.x = 0;
        matrix.y = 0;
    }
    
    void add (string[] names...)
    {
        foreach (ref name; names)
            push (Layer (name, shaders, this));
    }
}

struct Layer
{
    mixin (ShiftImpl!("isDefault", "setDefault", "undefault"));

    string name;
    Buffer buffer;
    
    this (string name, ShaderList* shaders, ref LayerSection owner)
    {
        this.name = name;
        buffer = Buffer (shaders,owner);
        setDefault ();
    }

    void add (ChildObject : GameObject)(ref ChildObject[] objects)
    {
        buffer.objects = cast(GameObject*) objects.ptr;
        buffer.objects.length = objects.length;
    }

    void add (ChildObject : GameObject)(ref ChildObject object)
    {
        buffer.objects = cast(GameObject*) &object;
        buffer.objects.length = 1;
    }

    void render ()
    {
        Engine.instance.sounds.checkMusic();
        buffer.bind ();
        Engine.instance.scene.attributes.init ();
        buffer.draw ();
    }
}

struct Buffer
{
    Texture texture;
    bool delegate() condition;
    mixin (ShiftImpl!("isInterface", "setInterface", "setObjects"));

    private
    {
        ShaderList* shaders;
        ObjectList objects;
        LayerSection* owner;

        GLuint VBO;
        Vertex[] vertexes;
    }
    
    this (ShaderList* shaders, ref LayerSection owner)
    {
        this.shaders = shaders;
        this.owner = &owner;
        setObjects ();
    }
    
    void create ()
    {
        size_t length;
        
        vertexes.length = objects.length * 4;

        foreach (size_t index; 0..objects.length)
        {
            auto object = objects[index];

            auto points    =  object.vertexPoints;
            auto textures  =  object.texturePoints;
            auto alpha     =  object.visibility;

            foreach (k; 0..4)
                vertexes[length + k] = Vertex (points[k], textures[k], alpha);
            
            length += 4;
        }

        generate ();
        texinit ();
    }

    void texinit ()
    {
        glUniform1i (shaders.base.data.uniforms.texture, 0);
    }

    void generate ()
    {
        glGenBuffers (1, &VBO);
    }
    
    void bind ()
    {
        glBindBuffer (GL_ARRAY_BUFFER, VBO);
        glBufferData (GL_ARRAY_BUFFER, Vertex.sizeof * vertexes.length, 
                      vertexes.ptr, GL_STATIC_DRAW);
        texture.bind (GL_TEXTURE0);
    }
    
    void draw ()
    {
        uint pointIndex;
        foreach (size_t index; 0..objects.length)
        {
            auto object = objects[index];

            object.animate ();

            foreach (k; 0..4)
            {
                vertexes[pointIndex + k].texturePoint = object.texturePoints[k];
                vertexes[pointIndex + k].alpha = object.visibility;
            }

            Engine.instance.scene.mxworld[0][3] = object.matrix.x 
                + (isInterface? 0 : owner.matrix.x);

            Engine.instance.scene.mxworld[1][3] = object.matrix.y 
                + (isInterface? 0 : owner.matrix.y);

            glUniformMatrix4fv (shaders.base.data.uniforms.world, 1, GL_TRUE, &Engine.instance.scene.mxworld[0][0]);
            
            if ((condition && condition ()) || !condition)
                glDrawArrays (GL_QUADS, pointIndex, 4);
            
            pointIndex += 4;
        }
    }
}

struct ObjectList
{
    GameObject* ptr;
    size_t length;

    alias ptr this;
}

struct Attributes
{
    void init ()
    {
        glVertexAttribPointer 
        (
            VERTEX_XYZ,
            VEC3,
            GL_FLOAT,
            GL_FALSE,
            Vertex.sizeof,
            cast (const (GLvoid)*) 0
        );
        
        glVertexAttribPointer 
        (
            TEXTURE_XY,
            VEC2,
            GL_FLOAT,
            GL_FALSE,
            Vertex.sizeof,
            cast (const (GLvoid)*) VertexPoint.sizeof
        );
        
        glVertexAttribPointer
        (
            ALPHA,
            SINGLE,
            GL_FLOAT,
            GL_FALSE,
            Vertex.sizeof,
            cast (const (GLvoid)*) (VertexPoint.sizeof + TexturePoint.sizeof)
        );
    }
    
    void enable ()
    {
        glEnableVertexAttribArray (VERTEX_XYZ);
        glEnableVertexAttribArray (TEXTURE_XY);
        glEnableVertexAttribArray (ALPHA);
    }
    
    void disable ()
    {
        glDisableVertexAttribArray (VERTEX_XYZ);
        glDisableVertexAttribArray (TEXTURE_XY);
        glDisableVertexAttribArray (ALPHA);
    }
}