﻿module dygren.graphics.point;

private import mathed : Planef, Stereof, Vector;

alias Stereof VertexPoint;
alias Planef TexturePoint;

alias PointSquare!VertexPoint SquareVertexPoints;
alias PointSquare!TexturePoint SquareTexturePoints;

struct Vertex
{
    VertexPoint   point;
    TexturePoint  texturePoint;
    float         alpha;
    
    this (VertexPoint point_, TexturePoint texturePoint_, float alpha_)
    {
        point         =  point_;
        texturePoint  =  texturePoint_;
        alpha         =  alpha_;
    }
}

private alias PointSquare(Type) = Vector!(4, Type, "upLeft,upRight,downRight,downLeft");