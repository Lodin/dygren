﻿module dygren.graphics.shader;

private 
{
    import dygren.utils.exceptions;
    import dygren.utils.shelf : NamedShelf;

    import derelict.opengl3.gl;
    import std.string : split, toStringz, format;
    import std.conv : to;
    import std.algorithm : canFind;
}

struct Shader
{
    Data data;
    private GLuint program;
    
    void create ()
    {
        program = glCreateProgram ();
        
        debug if (program == 0)
            throw new ShaderException (SHADER_PROGRAM_CREATING_FAILED);
    }
    
    void bind ()
    {
        foreach (uint index, ref var; data.vars)
            glBindAttribLocation (program, index, var.toStringz ());
    }
    
    void compile ()
    {
        foreach (GLuint type, ref code; data.list)
        {
            auto shader = ShaderObject (type, code);
            shader.compile ();
            shader.attach (program);
        }
    }
    
    void link ()
    {
        glLinkProgram (program);
        
        debug
        {
            GLint success;
            
            glGetProgramiv (program, GL_LINK_STATUS, &success);
            
            if (success == 0)
                throw new ShaderException (format (SHADER_PROGRAM_LINKING_FAILED, 
                                           program.log ()));
        }
    }
    
    void use ()
    {
        glUseProgram (program);
    }
    
    debug void validate ()
    {
        GLint success;
        
        glValidateProgram (program);
        glGetProgramiv (program, GL_VALIDATE_STATUS, &success);
        
        if (!success)
            throw new ShaderException (format (SHADER_PROGRAM_INVALID, program.log ()));
    }
    
    void test ()
    {
        foreach (string name, ref var; data.uniforms)
        {
            var = glGetUniformLocation (program, name.toStringz ());
            if (var == EMPTY_UNIFORM)
                throw new ShaderException (format (SHADER_UNIFORM_EMPTY, "name"));
        }
    }
    
    
private:
    
    struct Data
    {
        string[] vars;
        string vertex, fragment;
        Uniforms uniforms;
        
        @property string[GLenum] list ()
        {
            return [GL_VERTEX_SHADER: vertex, GL_FRAGMENT_SHADER: fragment];
        }
    }
    
    struct Uniforms
    {
        mixin NamedShelf!GLuint;

        void create (string[] names...)
        {
            foreach (ref name; names)
                push (name, GLuint.init);
        }
    }
    
    struct ShaderObject
    {
        private 
        {
            GLuint object;
            GLenum type;
            string code;
        }
        
        this (GLenum type, ref string code)
        {
            this.object = create (type);
            this.type = type;
            this.code = code;
        }
        
        void compile ()
        {
            GLchar* source = cast(char*) code.toStringz ();
            GLint sourceLength = cast(int) code.length;
            
            glShaderSource (object, 1, &source, &sourceLength);
            glCompileShader (object);
            
            debug
            {
                GLint success;
                glGetShaderiv (object, GL_COMPILE_STATUS, &success);
                
                if (!success)
                {
                    string typename;
                    if (type == GL_VERTEX_SHADER)
                        typename = "Vertex";
                    else
                        typename = "Fragment";
                    
                    throw new ShaderException (format (SHADER_COMPILING_FAILED,
                                               typename, object.log ()));
                }
            }
        }
        
        void attach (ref GLuint program)
        {
            glAttachShader (program, object);
        }
        
    private:
        
        GLuint create (GLenum type)
        {
            auto object = glCreateShader (type);
            debug if (object == 0)
                throw new ShaderException (format (SHADER_OBJECT_CREATING_FAILED,
                                                   type));
            
            return object;
        }
    }
}

private:

string log (ref GLuint object)
{
    GLint length;
    glGetShaderiv (object, GL_INFO_LOG_LENGTH, &length);
    
    GLchar[] _log;
    _log.length = length;
    glGetShaderInfoLog (object, to!int (char.sizeof * length), null, _log.ptr);
    return _log.to!string ();
}