﻿module dygren.graphics.texture;

private
{
    import dygren.utils.exceptions;
    
    import derelict.opengl3.gl;
    import derelict.sdl2.sdl : SDL_Surface, SDL_RWFromConstMem, SDL_GetError;
    import derelict.sdl2.image : IMG_LoadPNG_RW, IMG_Load;
    
    import std.conv : to;
    import std.string : format, toStringz;
}

class Texture
{
    private
    {
        GLenum target;
        GLuint object;
    }
    SDL_Surface* image;

    this (string source, GLenum target = GL_TEXTURE_2D)
    {
        this.target = target;
        load (source);
        configure ();
        create ();
    }

    this (ubyte[] texture, GLenum target = GL_TEXTURE_2D)
    {
        this.target = target;
        load (texture);
        configure ();
        create ();
    }

    this (SDL_Surface* image, GLenum target = GL_TEXTURE_2D)
    {
        this.image = image;
        this.target = target;
        configure ();
        create ();
    }
    
    void bind (GLenum unit)
    {
        glActiveTexture (unit);
        glBindTexture (target, object);
    }

    uint getPixelAlpha (float up, float left, float lengthX, float lengthY, float vertexCoodrsX, float vertexCoodrsY)
    {
        int zeroX = cast (int) (image.w * left),
            zeroY = cast (int) (image.h * up);

        auto countX = image.w * lengthX,
             countY = image.h * lengthY;

        auto pixelsByte = cast (int*) image.pixels;

        int textureCoordsX = cast (int) (vertexCoodrsX * countX) + zeroX,
            textureCoordsY = cast (int) (vertexCoodrsY * countY) + zeroY;

        int pixelIndex = textureCoordsX + textureCoordsY * image.w;
        auto alpha = (((pixelsByte[pixelIndex]) & (image.format.Amask)) >> image.format.Ashift) << image.format.Aloss;

        return alpha;

    }
    
private: 

    void load (string source)
    {
        image = IMG_Load (source.toStringz ());
        
        debug if (image == null)
            throw new TextureException (format (TEXTURE_LOADING_FAILED,
                                                SDL_GetError().to!string ()));
    }

    void load (ref ubyte[] texture)
    {
        image = IMG_LoadPNG_RW (SDL_RWFromConstMem (texture.ptr,
                                                    cast(int) texture.length));
        
        debug if (image == null)
            throw new TextureException (format (TEXTURE_LOADING_FAILED,
                                                SDL_GetError().to!string ()));
    }
    
    void configure ()
    {
        glGenTextures (1, &object);
        glBindTexture (target, object);
        
        glTexParameteri (target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri (target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }
    
    void create ()
    {
        glTexImage2D (target, 0, GL_RGBA, image.w, image.h, 0, GL_RGBA,
                      GL_UNSIGNED_BYTE, image.pixels);
    }
}