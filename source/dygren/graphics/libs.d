﻿module dygren.graphics.libs;

private
{
    import dygren.utils.exceptions;
    import dygren.utils.aliases : Size;
    
    import mathed : Vector;
    import derelict.sdl2.sdl;
    import derelict.sdl2.image;
    import derelict.sdl2.ttf;
    import derelict.sdl2.mixer;
    import derelict.opengl3.gl;
    
    import std.array : Appender;
    import std.conv : to;
    import std.string : toStringz, format;
}

struct Window
{
    string name;
    Size!ushort size;
    SDLFlags flags;
    
    private SDL_Window* _window = null;
    SDL_GLContext glcontext;

    @property 
    {
        Size!float mod ()
        {
            return Size!float (800f / size.w, 800f / size.h);
        }

        Size!ushort currentDisplay ()
        {
            SDL_DisplayMode display;
            SDL_GetCurrentDisplayMode (0, &display);
            
            return Size!ushort (cast(ushort) display.w, cast(ushort) display.h);
        }

        SDL_Window* ptr ()
        {
            return _window;
        }
    }


    void setFullscreen ()
    {
        SDL_DestroyWindow(_window);
        flags.fullscreen ();
    }

    void create ()
    {
        debug if (name == "")
            throw new LoopException (format (LOOP_WINDOW_NAME_NOT_DEFINED));

//        debug if (size == Size!ushort.init)
//            throw new LoopException (format (LOOP_WINDOW_SIZE_NOT_DEFINED));

        debug if (flags.list == 0)
            throw new LoopException (LOOP_FLAG_NOT_SET);

        size.set (800, 800);

        _window = SDL_CreateWindow
        (
            name.toStringz (),
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            cast(int) size.w,
            cast(int) size.h,
            flags.list
        );
        SDL_DisplayMode mode;
        SDL_GetWindowDisplayMode (this.ptr, &mode);
        SDL_SetWindowSize (this.ptr, mode.w, mode.h);
        SDL_SetWindowPosition(this.ptr, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
        int w, h;
        SDL_GetWindowSize(this.ptr,&w,&h);
        size.w = cast (ushort) w;
        size.h = cast (ushort) h;

        debug if (_window == null)
            throw new LoopException (format (LOOP_WINDOW_CREATION_FAILED, 
                                     SDL_GetError ().to!string ()));
    }
    
    void context () { SDL_GL_CreateContext (_window); }
    void swap () { SDL_GL_SwapWindow (_window); }
}

struct OGLib
{
    void load () { DerelictGL.load (); }
    void reload () { DerelictGL.reload (); }
    void init ()
    {
        glClearColor  (0.0f, 0.0f, 0.0f, 0.0f);
        glClear       (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable      (GL_ALPHA_TEST);
        glEnable      (GL_BLEND);
        glBlendFunc   (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
}

struct SDLib
{
    void load ()
    {
        DerelictSDL2.load ();
        DerelictSDL2Image.load ();
        DerelictSDL2ttf.load ();
        DerelictSDL2Mixer.load ();
    }
    
    void init ()
    {
        auto success = SDL_Init (SDL_INIT_VIDEO|SDL_INIT_AUDIO);
        
        debug if (success < 0)
            throw new LoopException (format (LOOP_SDL_INIT_FAILED, 
                                     SDL_GetError ().to!string ()));
        
        SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER,  1);
        SDL_GL_SetAttribute (SDL_GL_RED_SIZE,      5);
        SDL_GL_SetAttribute (SDL_GL_GREEN_SIZE,    6);
        SDL_GL_SetAttribute (SDL_GL_BLUE_SIZE,     5);
    }
}

private:

struct SDLFlags
{
    private int[11] flags;
    
    void fullscreen ()        { flags[0] = SDL_WINDOW_FULLSCREEN; }
    void fullscreenDesktop () { flags[1] = SDL_WINDOW_FULLSCREEN_DESKTOP; }
    void opengl ()            { flags[2] = SDL_WINDOW_OPENGL; }
    void hidden ()            { flags[3] = SDL_WINDOW_HIDDEN; }
    void borderless ()        { flags[4] = SDL_WINDOW_BORDERLESS; }
    void resizable ()         { flags[5] = SDL_WINDOW_RESIZABLE; }
    void minimized ()         { flags[6] = SDL_WINDOW_MINIMIZED; }
    void maximized ()         { flags[7] = SDL_WINDOW_MAXIMIZED; }
    void inputGrabbed ()      { flags[8] = SDL_WINDOW_INPUT_GRABBED; }
    void allowHighDPI ()      { flags[9] = SDL_WINDOW_ALLOW_HIGHDPI; }
    void shown ()             { flags[10] = SDL_WINDOW_SHOWN; }
    
    @property uint list ()
    {
        int result;
        
        foreach (ref flag; flags)
            result |= flag;
        
        return result;
    }
}