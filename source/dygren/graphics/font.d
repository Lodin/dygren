﻿module dygren.graphics.font;

private
{
    import dygren.utils.exceptions;
    import dygren.utils.util : hex2rgb;

    import derelict.sdl2.sdl : SDL_Surface, SDL_RWFromConstMem, SDL_Color;
    import derelict.sdl2.ttf;

    import std.string : format, toStringz;
    import std.conv : to;
}

struct Font
{
    FontStyle style;
    private TTF_Font* font;

    this (string source, ubyte size = 12)
    {
        init ();
        font = TTF_OpenFont (source.toStringz (), size);
        
        debug if (!font)
            throw new FontException (format (FONT_FILE_LOADING_FAILED,
                                             TTF_GetError().to!string ()));
    }

    this (ubyte[] raw, ubyte size = 12)
    {
        init ();
        font = TTF_OpenFontRW (SDL_RWFromConstMem (raw.ptr, cast(int) raw.length),
                               1, size);
        
        debug if (!font)
            throw new FontException (format (FONT_FILE_LOADING_FAILED,
                                             TTF_GetError().to!string ()));
    }

    SDL_Surface* render (string renderString)
    {
        debug if (!font)
            throw new FontException (FONT_NOT_DEFINED);

        if (renderString.length == 0)
            throw new FontException (FONT_RENDER_STRING_LENGTH_IS_NULL);

        return TTF_RenderUTF8_Blended (font, renderString.toStringz (),
                                       style.color);
    }

private:

    void init ()
    {
        if (TTF_WasInit ())
            return;

        if (TTF_Init () == -1)
            throw new FontException (format (FONT_LIBRARY_LOADING_FAILED,
                                             TTF_GetError().to!string ()));

        if (!style.owner)
            style.owner = &this;
    }
}

private:

struct FontStyle
{
    private
    {
        enum { NORMAL, BOLD, ITALIC, STRIKETHROUGH, UNDERLINE }
        enum { RED, GREEN, BLUE }

        bool[5] _style = [true, false, false, false, false];
        auto _color = SDL_Color (0, 0, 0);
        Font* owner;
    }

    @property
    {
        bool normal () { return _style[NORMAL]; }
        bool bold () { return _style[BOLD]; }
        bool italic () { return _style[ITALIC]; }
        bool strikethrough () { return _style[STRIKETHROUGH]; }
        bool underline () { return _style[UNDERLINE]; }

        SDL_Color color () { return _color; }
        void color (string hex)
        {
            debug if (hex.length != 6)
                throw new FontException (format (FONT_COLOR_NOT_ENOUGH,
                                                 hex.length));
            
            auto rgb = hex2rgb (hex);
            _color.r = rgb[RED];
            _color.g = rgb[GREEN];
            _color.b = rgb[BLUE];
        }
    }

    void set (string[] styles...)
    {
        int textStyle = TTF_STYLE_NORMAL;

        foreach (ref style; styles)
        {
            switch (style)
            {
                case "normal":
                    textStyle = TTF_STYLE_NORMAL;
                    _style[NORMAL] = true;
                    _style[BOLD] = false;
                    _style[ITALIC] = false;
                    _style[UNDERLINE] = false;
                    break;
                case "bold":
                    textStyle |= TTF_STYLE_BOLD;
                    _style[NORMAL] = false;
                    _style[BOLD] = true;
                    break;
                case "italic":
                    textStyle |= TTF_STYLE_ITALIC;
                    _style[NORMAL] = false;
                    _style[ITALIC] = true;
                    break;
                case "strikethrough":
                    textStyle |= TTF_STYLE_STRIKETHROUGH;
                    _style[NORMAL] = false;
                    _style[STRIKETHROUGH] = true;
                    break;
                case "underline":
                    textStyle |= TTF_STYLE_UNDERLINE;
                    _style[NORMAL] = false;
                    _style[UNDERLINE] = true;
                    break;
                default:
                    throw new FontException (format (FONT_STYLE_NOT_EXIST,
                                                     style));
            }
        }

        TTF_SetFontStyle (owner.font, textStyle);
    }
}