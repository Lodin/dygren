﻿module dygren.graphics.glsl.base;

private import dygren.graphics.shader : Shader;

enum 
{
    baseVertex = q{
        #version 130
        
        in vec3 vertexXYZ;
        in vec2 textureXY;
        in float alphaIn;
        
        uniform mat4 world;
        
        out vec2 texturePosition;
        out float alpha;
        
        void main()
        {
            gl_Position = world * vec4 (vertexXYZ, 1.0);
            texturePosition = textureXY;
            alpha = alphaIn;
        }
    },
    
    baseFragment = q{
        #version 130
        
        in vec2 texturePosition;
        in float alpha;
        
        uniform sampler2D texture;
        
        out vec4 fragmentColor;
        
        vec4 changeAlpha (vec4 color)
        {
            return vec4 (color.r, color.g, color.b, color.a * alpha);
        }
        
        void main ()
        {
            vec4 buffColor = texture2D (texture, texturePosition);
            fragmentColor = changeAlpha (buffColor);
        }
    }
}

Shader baseShader;

static this ()
{
    baseShader.data.vertex = baseVertex;
    baseShader.data.fragment = baseFragment;
    baseShader.data.vars = ["vertexXYZ", "textureXY", "alpha"];
    baseShader.data.uniforms.create ("world", "texture");
}