﻿module dygren.menuButtons.buttons;

private
{    
    import dygren.core.engine : Engine;
    import dygren.objects.gameobject : GameObject;
    import dygren.graphics.point : SquareVertexPoints, SquareTexturePoints, VertexPoint;
    import dygren.utils.shelf : OrderedNamedShelf, NamedShelf, Shelf;
    import dygren.utils.aliases : Size;
    import dygren.utils.util : OGLAxes;
    import dygren.graphics.texture : Texture;
    
    import dygren.core.scene : Layer;
    
    import mathed : Vector, Planef;
    
    import std.string : format;
    import std.traits : hasMember;
    
}
alias Vector!(4, float, "top,bottom,left,right") Margin;
alias Vector!(3, bool, "left,center,right") Align;
alias bool delegate() Action;


struct ButtonTextures
{
    SquareTexturePoints textureActive;
    SquareTexturePoints textureDeactive;
    this (SquareTexturePoints deactive, SquareTexturePoints active)
    {
        textureActive = active;
        textureDeactive = deactive;
    }
    
    SquareTexturePoints activated ()
    {
        return textureActive;
    }
    
    SquareTexturePoints deactivated ()
    {
        return textureDeactive;
    }
}

struct ButtonList
{
    mixin OrderedNamedShelf!Button;
    
    void add (string name, Action action, ButtonTextures buttonTextures, bool buttonGo = true)
    {
        push (new Button (name, action, buttonTextures, buttonGo));
    }
}

class Button : GameObject
{
    string name;
    Action action;
    ButtonTextures buttonTextures;
    bool current;
    bool buttonGo;
    
    this (string name, Action action, ButtonTextures buttonTextures, bool buttonGo = true)
    {
        this.name = name;
        this.action = action;
        this.buttonTextures = buttonTextures;
        this.texturePoints = buttonTextures.deactivated;
        this.buttonGo = buttonGo; 
    }
    
    void activated ()
    {
        this.texturePoints = buttonTextures.activated;
    }
    
    void deactivated ()
    {
        this.texturePoints = buttonTextures.deactivated;
    }

    void reactive ()
    {
        if (this.texturePoints == buttonTextures.activated)
            this.texturePoints = buttonTextures.deactivated;
        else
            this.texturePoints = buttonTextures.activated;
    }

    void setTextureButton (int mouseX, int mouseY, Texture menuTexture)
    {
        unsetButton ();
        if (Engine.instance.scene.getGameObject(this, mouseX, mouseY, menuTexture, 0, 0))
        {
            current = true;
            if (buttonGo)
                this.activated();
            return;
        }
    }
    
    void unsetButton ()
    {
        if (current)
        {
            if (buttonGo)
                this.deactivated;
            current = false;
        }
    }
}

struct ButtonData
{
    Alignment alignment;
    auto margin = Margin (0.1, 0.1, 0.5, 0.5);
    auto interval = 0.4;
    float modW, modH;
    bool vertical;
    bool isSquare;
    string currentButton;
    string[] buttonsName;
    
    ButtonList buttons;
    
    void init (Margin menuMargin, float buttonsInterval, bool isSquare = false, float modW = 1, float modH = 1, bool vertical = true)
    {
        margin = menuMargin;
        interval = buttonsInterval;
        this.modW = modW;
        this.modH = modH;
        this.vertical= vertical;
        this.isSquare = isSquare;
    }
    
    void compute ()
    {
        foreach (size_t index, ref button; buttons)
            button.vertexPoints = computeButton (index);
    }

    void setCurrentButton (int mouseX, int mouseY, Texture menuTexture)
    {        
        unsetButton();
        foreach (ref name; buttonsName)
        {
            if (Engine.instance.scene.getGameObject(buttons[name], mouseX, mouseY, menuTexture, 0, 0))
            {
                if (buttons[name].buttonGo)
                    buttons[name].activated;
                currentButton = name;
                return;
            }
        }
    }
    
    void unsetButton ()
    {
        if (currentButton != "")
        {
            if (buttons[currentButton].buttonGo)
                buttons[currentButton].deactivated;
            currentButton = "";
        }
    }
    
private:
    
    @property
    {                
        Size!float box ()
        {
            return Size!float
                (
                    OGLAxes.x.length - (margin.left + margin.right),
                    OGLAxes.y.length - (margin.top + margin.bottom)
                    );
        }
    }
    
    //SquareVertexPoints computeButton (size_t index, float gap)
    SquareVertexPoints computeButton (size_t index)
    {
        float
            //positionX = alignX,
            positionX = OGLAxes.x.start + margin.left,
            positionY = OGLAxes.y.end - margin.top,
            positionZ = OGLAxes.z.start;
        
        float width, height;
        if (vertical)
        {
            width = box.w;
            height = (box.h - interval * (buttons.count - 1)) / buttons.count; 
        }
        else
        {
            width = (box.w - interval * (buttons.count - 1)) / buttons.count;
            height = box.h; 
            
        }
        if (index != 0)
        {
            if (vertical)
                positionY -= (height + interval) * index;
            else
                positionX += (width + interval) * index;
        }
        if (isSquare)
        {
            if (width > height)
                width = height * modW / modH;
            else
                height = width * modH / modW;
        }
        auto result = SquareVertexPoints
            (
                VertexPoint (positionX, positionY, positionZ), // up-left
                VertexPoint (positionX + width, positionY, positionZ), // up-right
                VertexPoint (positionX + width, positionY - height, positionZ), // down-right
                VertexPoint (positionX, positionY - height, positionZ) // down-left
                );
        
        return result;
    }
    
}

struct Alignment
{
    private auto _align = Align (false, true, false);
    
    @property string current ()
    {
        if (_align.left)
            return "left";
        else if (_align.center)
            return "center";
        else
            return "right";
    }
    
    @property Align position () { return _align; }
    
    void left () { _align.set (true, false, false); }
    void center () { _align.set (false, true, false); }
    void right () { _align.set (false, false, true); }
}