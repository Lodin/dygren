﻿module dygren.sounds.sounds;
private
{

    import derelict.sdl2.mixer;
    import derelict.sdl2.types;
    import derelict.sdl2.sdl;
    
    import dygren.utils.exceptions;

    import std.string : format, toStringz;
}
class Sounds 
{
    bool canMusic, canChunk;
    //Музыка, которую будем проигрывать
    Mix_Music*[string] music;
    string[] musicName;
    //Звуки, которые будут использоваться
    Mix_Chunk*[string] chunk;
    string[] chunkName;
    //Специальная музыка, которую будем проигрывать в особые моменты (например, боссы)
    Mix_Music*[string] musicSpec;
    string[] musicSpecName;

    int musicLoopIndex = 0;

    this() 
    {
        // Настраиваем mp3 (и другие форматы)
//        int flags=MIX_INIT_MP3;
//        int initted=Mix_Init(~0);
        //if(initted&flags != flags) {
        //    throw new SoundException (SOUND_NOT_FIND);
        //}
        //Инициализировать SDL_mixer
        if(Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1) {
            throw new SoundException (SOUND_NOT_FIND);
        }
    }

    void loadMusic(string[][] allMusicFile,string[][] allChunkFile, string[][] allMusicSpecFile)//, short*[] allSounds)
    {    
        canMusic = true;
        canChunk = true;
        //Загружаем музыку
        musicName = allMusicFile[0];
        foreach (size_t index, ref musicFile; musicName)
        { 
            music[musicFile] = Mix_LoadMUS(allMusicFile[1][index].toStringz ());
            //Если не получилось
            if(music[musicFile] == null)
            {
                throw new SoundException (SOUND_NOT_FIND);
            }
        }
        //Загружаем звуковые эффекты
        chunkName = allChunkFile[0];
        foreach (size_t index, ref chunkFile; chunkName)
        { 
            chunk[chunkFile] = Mix_LoadWAV(allChunkFile[1][index].toStringz ());
            //Если не получилось
            if(chunk[chunkFile] == null)
            {
                throw new SoundException (SOUND_NOT_FIND);
            }
        }

        musicSpecName = allMusicSpecFile[0];
        foreach (size_t index, ref musicSpecFile; musicSpecName)
        { 
            musicSpec[musicSpecFile] = Mix_LoadMUS(allMusicSpecFile[1][index].toStringz ());
            //Если не получилось
            if(musicSpec[musicSpecFile] == null)
            {
                throw new SoundException (SOUND_NOT_FIND);
            }
        }
        checkMusic();
    }

    void playMusic()
    {
        //If there is no music playing
//        if( Mix_PlayingMusic() == 0 )
//        {
            //Play the music
//            if( Mix_PlayMusic( music[0], -1 ) == -1 )
//            {
//                return;
//            }
        if (canMusic)
        {
            if (musicName.length == 1)
                musicLoopIndex = 0;
            else
                musicLoopIndex = (musicLoopIndex + 1)%musicName.length;
            Mix_FadeInMusic(music[musicName[musicLoopIndex]],1,1000);
        }
//        }
        //If music is being played
//        else
//        {
//            //If the music is paused
//            if( Mix_PausedMusic() == 1 )
//            {
//                //Resume the music
//                Mix_ResumeMusic();
//            }
//            //If the music is playing
//            else
//            {
//                //Pause the music
//                Mix_FadeOutMusic(1000);
//            }
//        }
    }

    void playMusic(string musicName)
    {
        if( Mix_PlayingMusic() != 0 )
            stopMusic();
        Mix_PlayMusic(musicSpec[musicName],-1);  
    }

    void stopMusic()
    {
        if( Mix_PausedMusic() != 1 )
            Mix_PauseMusic();
    }
    
    void playChunk(string chunkName)
    {
        Mix_PlayChannel( -1, chunk[chunkName], 0 );
    }

    void checkMusic()
    {
        if ( Mix_PlayingMusic() == 0 )
            playMusic();
//        if (wait == 0)
//        {
//            playMusic();
//            return;
//        }
    }

    void pauseMusic()
    {
        if (canMusic)
        {
            if( Mix_PausedMusic() != 1 )
            {
            canMusic = false;
            Mix_PauseMusic();
            }
        }
        else
        {
            canMusic = true;
            Mix_ResumeMusic();
        }
    }

//    void pauseMusic()
//    {
//        if( Mix_PausedMusic() != 1 )
//        {
//            canMusic = false;
//            Mix_PauseMusic();
//        }
//        else
//        {
//            canMusic = true;
//            Mix_ResumeMusic();
//        }
//    }

    void clear()
    {
        //Free the sound effects
        foreach (ref name; chunkName)
            Mix_FreeChunk( chunk[name] );

        //Free the music
        foreach (ref name; musicName)
            Mix_FreeMusic( music[name] );
        
        //Free the musicSpec
        foreach (ref name; musicSpecName)
            Mix_FreeMusic( musicSpec[name] );
    }
}