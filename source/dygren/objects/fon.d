﻿module dygren.objects.fon;

private
{
    import dygren.graphics.point : SquareVertexPoints, SquareTexturePoints, VertexPoint;
    import dygren.objects.gameobject : GameObject;
    import dygren.utils.util : OGLAxes;
}

class GameFon : GameObject
{
    this (SquareTexturePoints texture, SquareVertexPoints vertex)
    {
        super ();
        texturePoints = texture;
        vertexPoints = vertex;
    }

    bool checkCoords (float x, float y)
    {
        if (x > 0 ||  x < vertexPoints.upLeft.x + OGLAxes.x.length - vertexPoints.upRight.x)
            return false;
        if (y < 0 ||  y > vertexPoints.upLeft.y - OGLAxes.y.length - vertexPoints.downLeft.y)
            return false;
        return true;
    }
    
    void ReplaceCoords (ref float x, ref float y)
    {
        if (x > 0)
            x = 0;
        else if (x < vertexPoints.upLeft.x + OGLAxes.x.length - vertexPoints.upRight.x)
            x = vertexPoints.upLeft.x + OGLAxes.x.length - vertexPoints.upRight.x;
        if (y < 0)
            y = 0;
        else if (y > vertexPoints.upLeft.y - OGLAxes.y.length - vertexPoints.downLeft.y)
            y = vertexPoints.upLeft.y - OGLAxes.y.length - vertexPoints.downLeft.y;
    }
}