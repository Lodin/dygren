﻿module dygren.objects.gameobject;

private
{
    import dygren.graphics.point : SquareVertexPoints, SquareTexturePoints;
    import dygren.utils.time : Frames;
    import dygren.utils.shift : ShiftImpl;
    import dygren.utils.refshelf : RefShelf;
    import dygren.utils.shelf : NamedShelf,Shelf;

    import mathed : Planef;
}

class GameObject
{
    SquareVertexPoints vertexPoints;
    SquareTexturePoints  texturePoints;
    float visibility = 1.0;
    Planef matrix = Planef (0, 0);
    Planef speed = Planef (0, 0);
    mixin (ShiftImpl!("isObject", "object", "fon"));
    Animation animation;

    this ()
    {
        object();
    }

    protected Frames frames;

    @property bool hasType (Type)() { return this.classinfo == typeid (Type); }

    void animate () { return; }
    void stop () { return; }
    void returnFps (string nameAnimation, ref size_t step, ref size_t anim)
    {
        animation.returnFps (nameAnimation, step, anim, this.texturePoints, this.visibility);
    }

    void returnNextStepFps ()
    {
        animation.returnNextStepFps (this.texturePoints, this.visibility);
    }

    void returnNextAnimFps ()
    {
        animation.returnNextAnimFps (this.texturePoints, this.visibility);
    }

    void returnNewAnimationFps (string nameAnimation)
    {
        animation.returnNewAnimationFps (nameAnimation, this.texturePoints, this.visibility);
    }
}

struct Animation
{
    mixin NamedShelf!FpsStep;
    size_t step;
    size_t anim;
    string nameAnimation;
    
    void addAnimate (string name, SquareTexturePoints[] step, float[] visibility)
    {
        foreach (ref nameAnimate; this.list)
        {
            if (nameAnimate == name)
            {
                this[name].addStep(step, visibility);
                return;
            }
        }
        this.push(name, FpsStep(step, visibility));
    }
    
    void returnFps (string nameAnimation, size_t step, size_t anim, ref SquareTexturePoints texture, ref float visibility)
    {
        this.nameAnimation = nameAnimation;
        this.step = step;
        this.anim = anim;
        texture = this[nameAnimation][step][anim].texture;
        visibility = this[nameAnimation][step][anim].visibility;
    }
    
    void returnNextStepFps (ref SquareTexturePoints texture, ref float visibility)
    {
        if (this[nameAnimation].count == 1)
            step = 0;
        else
            step = (step + 1) % this[nameAnimation].count;
        texture = this[nameAnimation][step][anim].texture;
        visibility = this[nameAnimation][step][anim].visibility;  
    }
    
    void returnNextAnimFps (ref SquareTexturePoints texture, ref float visibility)
    {
        if (this[nameAnimation][step].count == 1)
            anim = 0;
        else
            anim = (anim + 1) % this[nameAnimation][step].count;
        texture = this[nameAnimation][step][anim].texture;
        visibility = this[nameAnimation][step][anim].visibility;     
    }
    
    void returnNewAnimationFps (string nameAnimation, ref SquareTexturePoints texture, ref float visibility)
    {
        this.nameAnimation = nameAnimation;
        step = 0;
        texture = this[nameAnimation][step][anim].texture;
        visibility = this[nameAnimation][step][anim].visibility;     
    }
}

struct Fps
{
    SquareTexturePoints texture;
    float visibility;
    
    this (SquareTexturePoints texture, float visibility)
    {
        this.texture = texture;
        this.visibility = visibility;
    }
}

struct FpsAnim
{
    mixin Shelf!Fps;
    //
    //    this (SquareTexturePoints[] anim, float[] visibility)
    //    {
    //        foreach (size_t index, ref fps; anim)
    //            add (Fps(fps, visibility[index]));
    //    }
    //
    this (SquareTexturePoints anim, float visibility)
    {
        addAnim (anim, visibility);
    }
    
    void addAnim (SquareTexturePoints anim, float visibility)
    {
        this.push(Fps(anim, visibility));
    }
}

struct FpsStep
{
    mixin Shelf!FpsAnim;
    
    this (SquareTexturePoints[] step, float[] visibility)
    {
        addStep (step, visibility);
    }
    
    void addStep (SquareTexturePoints[] step, float[] visibility)
    {
        this.push (FpsAnim(step[0], visibility[0]));
        auto indexStep = this.count - 1;
        int index;
        if (step.length <= visibility.length)
        {
            for (index = 1; index < step.length; index++)
                this[indexStep].addAnim (step[index], visibility[index]);
            for ( ; index < visibility.length; index++)
                this[indexStep].addAnim (step[step.length - 1], visibility[index]);
        }
        else
        {
            for (index = 1; index < visibility.length; index++)
                this[indexStep].addAnim (step[index], visibility[index]);
            for ( ; index < step.length; index++)
                this[indexStep].addAnim (step[index], visibility[visibility.length - 1]);
        }
        
    }
}